﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DataContract.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ServiceContract.Authentication;
using WebApi.Config.Extentions;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        IUserService _userService;
        private readonly AppSettings _appSettings;
        public UsersController(IUserService userService,IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
            _userService = userService;
        }
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public UserDto Authenticate([FromBody]UserLoginDto userParam)
        {
            var userDto= _userService.Authenticate(userParam.Email, userParam.Password);
            if (userDto == null)
                throw new Exception("Username or Password Invalid");
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var claims = new ClaimsIdentity();
            claims.AddClaims(new[]
                {
                    new Claim("UserId", userDto.Id.ToString())
                });
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = claims,
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                  SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
           userDto.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            userDto.Password = null;
            userDto.Id = 0;
            return userDto;
        }
    }
}