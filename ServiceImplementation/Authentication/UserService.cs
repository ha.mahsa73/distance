﻿using AutoMapper;
using DataContract.Authentication;
using PersistanceCore;
using ServiceContract.Authentication;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceImplementation.Authentication
{
    public class UserService : IUserService
    {
        IUserRepository _userRepository;
        IMapper _mapper;
        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public UserDto Authenticate(string email, string password)
        {
            var user = findByEmailAddress(email);
            //In the real environment the password is stored as encrypted
            if (user != null)
            {
                if (user.Password == password)
                {
                    return user;
                }
                return null;
            }
            else
                return null;

        }

        public UserDto findByEmailAddress(string email)
        {
            var model = _userRepository.FindByEmailAddress(email);
            var dto= _mapper.Map<UserDto>(model);
            return dto;

        }

        public UserDto Insert(UserDto model)
        {
            throw new NotImplementedException();
        }
    }
}
