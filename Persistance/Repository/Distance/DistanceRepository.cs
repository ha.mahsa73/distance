﻿using DomainModel;
using Persistance.Facade;
using PersistanceCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistance.Repository
{
    public class DistanceRepository : IDistanceRepository

    {
        DistanceContext _context;
        public DistanceRepository()
        {
            _context = new DistanceContext();
        }
        public void SaveInDataBase(Distance distance)
        {
            _context.Distances.Add(distance);
            _context.SaveChanges();
        }
    }
}
