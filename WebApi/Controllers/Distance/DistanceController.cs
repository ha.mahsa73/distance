﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DataContract.Distance;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceContract.Distance;
using WebApi.Config.Rabbit;

namespace WebApi.Controllers.Distance
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DistanceController : ControllerBase
    {
        private IRabbitManager _manager;
        IDistanceService _distanceService;
        public DistanceController(IRabbitManager manager, IDistanceService distanceService)
        {
            _manager = manager;
            _distanceService = distanceService;
        }
        [HttpPost]

        public double CalcuteDistance(DistanceViewModel dto )
        {

            string userId = User.FindFirst("UserId").Value;

            var dis = _distanceService.GetDistanceBetweenPoints(dto.Lat1, dto.long1, dto.Lat2, dto.long1);

            // publish message  
           
            _manager.Publish(new {lat1 = dto.Lat1,long1 = dto.long1,lat2= dto.Lat2, long2= dto.long2, Dis= dis,UserId=int.Parse(userId)},
                "Distance", "", "Distance","Distance");
          
            return dis;
        }

    }
}