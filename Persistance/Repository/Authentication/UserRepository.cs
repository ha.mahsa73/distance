﻿using DomainModel;
using Persistance.Facade;
using PersistanceCore;
using System;
using System.Linq;

namespace Persistance
{
    public class UserRepository : IUserRepository
    {
        DistanceContext _context;
        public UserRepository()
        {
            _context = new DistanceContext();
        }
      

        public User FindByEmailAddress(string email)
        {
           return _context.Users.FirstOrDefault(x => x.EmailAddress == email);
        }
    }
}
