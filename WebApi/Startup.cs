using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Authentication.Config.Extentions;
using AutoMapper;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Persistance;
using Persistance.Repository;
using PersistanceCore;
using ServiceContract.Authentication;
using ServiceContract.Distance;
using ServiceImplementation.Authentication;
using ServiceImplementation.Distance;
using ServiceImplementation.Mapping;
using WebApi.Config.Extentions;
using WebApi.Config.Rabbit;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddOurSwaager();
            //Rabbit
            services.AddRabbit(Configuration);

            services.AddMvc(option => option.EnableEndpointRouting = false);
            // Automapper
           
            services.AddAutoMapper(c => c.AddProfile<AutoMapping>(), typeof(Startup));
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddOurAuthentication(appSettings);

            // DI services
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IDistanceService, DistanceService>();
            services.AddScoped<IDistanceRepository, DistanceRepository>();
            services.AddScoped<IRabbitDatabase, RabbitDatabase>();


            services.AddAutoMapper(typeof(Startup));

            // Add Hangfire services.
            services.AddHangfire(x => x.UseSqlServerStorage(Configuration.GetConnectionString("DefaultConnection")));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IBackgroundJobClient backgroundJobs, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1.0");

            });
            app.UseCors(x => x
               .AllowAnyOrigin()
               .AllowAnyMethod()
               .AllowAnyHeader());
            app.UseHangfireDashboard();

            RecurringJob.AddOrUpdate<IRabbitDatabase>(x => x.SaveAllData("Distance"),
              "*/20 * * * * *");
            RecurringJob.AddOrUpdate(
         () => Console.WriteLine("Delayed!"),
         Cron.Minutely);
            app.UseHangfireServer();
           
            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();
            app.UseDeveloperExceptionPage();
                      
          
        }
    }
}
