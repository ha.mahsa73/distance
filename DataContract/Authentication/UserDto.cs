﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataContract.Authentication
{
   public class UserDto
    {
        public int Id { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
      public string Token { get; set; }
    }
}
