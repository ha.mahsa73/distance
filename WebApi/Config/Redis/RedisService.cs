﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using NLog.Fluent;
using StackExchange.Redis;

namespace Authentication.Config.Redis
{
    public class RedisService
    {
        private readonly string _redisHost;
        private readonly int _redisPort;
        private ConnectionMultiplexer _redis;
        public RedisService(IConfiguration config)
        {
            _redisHost = config["Redis:Host"];

            _redisPort = Convert.ToInt32(config["Redis:Port"]);
            //_instanceName = config.InstanceName;
        }
        public void Connect()
        {
            try
            {
                var configString = $"{_redisHost}:{_redisPort},connectRetry=5";
                _redis = ConnectionMultiplexer.Connect(configString);
            }
            catch (RedisConnectionException err)
            {
                Log.Error(err.ToString());
                throw err;
            }
            Log.Debug($"Connected to Redis");
        }
        public  void Set(string key, string value)
        {
            var db = _redis.GetDatabase(); 
            db.StringSet(key, value);
        }

        public string Get(string key)
        {
            var db = _redis.GetDatabase();
            return  db.StringGet(key);
        }
    }

}
