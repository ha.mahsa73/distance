﻿using DataContract.Distance;
using Microsoft.Extensions.ObjectPool;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceContract.Distance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Config.Rabbit
{
    public class RabbitManager : IRabbitManager
    {
        private readonly DefaultObjectPool<IModel> _objectPool;
       
       
        public RabbitManager(IPooledObjectPolicy<IModel> objectPolicy)
        {
            _objectPool = new DefaultObjectPool<IModel>(objectPolicy, Environment.ProcessorCount * 2);
         
        }

        public void Publish<T>(T message, string exchangeName, string exchangeType, string routeKey,string queue)
            where T : class
        {
            if (message == null)
                return;

            var channel = _objectPool.Get();

            try
            {
                channel.QueueDeclare(queue: queue, durable: false, exclusive: false, autoDelete: false, arguments: null);

                var sendBytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));

              

                channel.BasicPublish("", routeKey, null, sendBytes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //_objectPool.Return(channel);
            }
        }

       
    }
}
