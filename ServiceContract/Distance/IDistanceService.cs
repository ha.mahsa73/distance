﻿using DataContract.Distance;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceContract.Distance
{
   public interface IDistanceService
    {
        double GetDistanceBetweenPoints(double lat1, double long1, double lat2, double long2);
        void SaveInDataBase(DistanceDto model);
      
    }
}
