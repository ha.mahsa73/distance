﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersistanceCore
{
    public interface IDistanceRepository
    {
        void SaveInDataBase(Distance distance);
    }
}
