﻿using DomainModel;
using Microsoft.EntityFrameworkCore;
using Persistance.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistance.Facade
{
   public class DistanceContext : DbContext
    {
        public DistanceContext()
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // optionsBuilder.UseSqlServer(@"Server=150.150.100.153;Database=EEE;User ID=sa;Password=Aa123456;Trusted_Connection=False;");
            optionsBuilder.UseSqlServer(@"Server=localhost;Database=Distance;User ID=sa;Password=Aa123456
              ;Trusted_Connection=False;", options => options.EnableRetryOnFailure());

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
           //Mapping
            modelBuilder.ApplyConfiguration(new UserMapping());
            modelBuilder.ApplyConfiguration(new DistanceMapping());
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Distance> Distances { get; set; }
    }
}
