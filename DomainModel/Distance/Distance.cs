﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
   public class Distance
    {
        public long Id { get; set; }
        public int UserId { get; set; }
        public double Lat1 { get; set; }
        public double long1 { get; set; }
        public double Lat2 { get; set; }
        public double long2 { get; set; }
        public double Dis { get; set; }
        public virtual User User { get; set; }
    }
}
