﻿namespace WebApi.Controllers.Distance
{
    public class DistanceViewModel
    {
        public double Lat1 { get; set; }
        public double long1 { get; set; }
        public double Lat2 { get; set; }
        public double long2 { get; set; }
        public double Dis { get; set; }
    }
}