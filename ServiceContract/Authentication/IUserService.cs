﻿using DataContract.Authentication;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceContract.Authentication
{
    public interface IUserService
    {
        UserDto Insert(UserDto model);
        UserDto findByEmailAddress(string email);
        UserDto Authenticate(string email, string password);

    }
}
