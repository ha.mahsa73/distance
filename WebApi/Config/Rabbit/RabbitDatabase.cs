﻿using DataContract.Distance;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceContract.Distance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Config.Rabbit
{
    public interface IRabbitDatabase
    {
        void SaveAllData(string queue);
    }
    public class RabbitDatabase: IRabbitDatabase
    {
        readonly IDistanceService _distanceService;
        public RabbitDatabase( IDistanceService distanceService)
        {
           
            _distanceService = distanceService;
        }

        public void SaveAllData(string queue)
        {
            IList<string> messages = new List<string>();
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: queue, durable: false, exclusive: false, autoDelete: false, arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    messages.Add(message);
                };
                channel.BasicConsume(queue: queue, autoAck: true, consumer: consumer);


            }
            if (queue == "Distance")
            {
                foreach (var item in messages)
                {
                    DistanceDto dto = Newtonsoft.Json.JsonConvert.DeserializeObject<DistanceDto>(item);
                    _distanceService.SaveInDataBase(dto);

                }
            }
        }
    }
}
