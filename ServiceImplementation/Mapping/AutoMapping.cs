﻿using AutoMapper;
using DataContract.Authentication;
using DataContract.Distance;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceImplementation.Mapping
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<User, UserDto>(); 
            CreateMap<UserDto, User>();
            CreateMap<DomainModel.Distance, DistanceDto>();
             CreateMap<DistanceDto,DomainModel. Distance>();


        }
    }
}
