﻿using AutoMapper;
using DataContract.Distance;
using PersistanceCore;
using ServiceContract.Distance;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceImplementation.Distance
{
    public class DistanceService : IDistanceService
    {

        IDistanceRepository _distanceRepository;
        IMapper _mapper;
        public DistanceService(IDistanceRepository distanceRepository, IMapper mapper)
        {
            _distanceRepository = distanceRepository;
            _mapper = mapper;
        }
        public double GetDistanceBetweenPoints(double lat1, double long1, double lat2, double long2)
        {
            double distance = 0;

            double dLat = (lat2 - lat1) / 180 * Math.PI;
            double dLong = (long2 - long1) / 180 * Math.PI;

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2)
                        + Math.Cos(lat2) * Math.Sin(dLong / 2) * Math.Sin(dLong / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            //Calculate radius of earth
            // For this you can assume any of the two points.
            double radiusE = 6378135; // Equatorial radius, in metres
            double radiusP = 6356750; // Polar Radius

            //Numerator part of function
            double nr = Math.Pow(radiusE * radiusP * Math.Cos(lat1 / 180 * Math.PI), 2);
            //Denominator part of the function
            double dr = Math.Pow(radiusE * Math.Cos(lat1 / 180 * Math.PI), 2)
                            + Math.Pow(radiusP * Math.Sin(lat1 / 180 * Math.PI), 2);
            double radius = Math.Sqrt(nr / dr);

            //Calaculate distance in metres.
            distance = radius * c;
            return distance;
        }

        public void SaveInDataBase(DistanceDto model)
        {
            var dto = _mapper.Map<DomainModel.Distance>(model);
            _distanceRepository.SaveInDataBase(dto);
        }
    }
}
