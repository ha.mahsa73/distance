﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
   public class User
    {
        public int Id { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public virtual IList<Distance>Distances { get; set; }
    }
}
