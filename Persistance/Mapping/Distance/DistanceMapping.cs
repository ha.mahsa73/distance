﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using DomainModel;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistance.Mapping
{
   public class DistanceMapping : IEntityTypeConfiguration<Distance>
    {
        public void Configure(EntityTypeBuilder<Distance> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(t => t.Lat2).IsRequired();
            builder.Property(t => t.Lat1).IsRequired();
            builder.Property(t => t.long1).IsRequired();
            builder.Property(t => t.long2).IsRequired();
            builder.Property(t => t.Dis).IsRequired();
            builder
              .HasOne<User>(sc => sc.User)
              .WithMany(s => s.Distances)
              .HasForeignKey(sc => sc.UserId);

        }

       
    }
}
